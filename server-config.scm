;; This is an operating system configuration
;; for a server machine and is a work in progress.

(use-modules
 (gnu)
 (gnu packages admin)
 (gnu packages avahi)
 (gnu packages bash)
 (gnu packages certs)
 (gnu packages cups)
 (gnu packages curl)
 (gnu packages dns)
 (gnu packages file)
 (gnu packages fonts)
 (gnu packages fontutils)
 (gnu packages freedesktop)
 (gnu packages glib)
 (gnu packages linux)
 (gnu packages lsof)
 (gnu packages ncurses)
 (gnu packages perl)
 (gnu packages rsync)
 (gnu packages sqlite)
 (gnu packages ssh)
 (gnu packages suckless)
 (gnu packages version-control)
 (gnu packages video)
 (gnu packages web)
 (gnu packages wm)
 (gnu packages xorg)
 (gnu packages xdisorg)
 (srfi srfi-1))


(use-service-modules
 admin avahi cups dbus dns networking pm rsync ssh web xorg)

(define %tty-services
  ;; Services that are too many. Why does anyone need so many ttys?
  ;; There must be some case where it makes sense but not here.
  (list console-font-service-type
	mingetty-service-type
	agetty-service-type))

(define %mybase
  (remove (lambda (service)
	    (eq? (service-kind service) mingetty-service-type)
	    (eq? (service-kind service) agetty-service-type)
	    (eq? (service-kind service) console-font-service-type))
	  %base-services))

(define %mykb
   (keyboard-layout "us" "altgr-intl")  
   )

(define %minesvc
  ;;
  (append
   (list (service dropbear-service-type
		  (dropbear-configuration
		   (port-number 22))
		  )
	 (service cups-service-type)
	 (service dbus-root-service-type)
	 (service httpd-service-type
		  (httpd-configuration
		   (config
		    (httpd-config-file
		     (server-name "io")
		     (listen '("192.168.12.250:80"))))))
	 ;;(static-networking-service "enp5s0.0" "192.168.12.240"
	;;			    #:netmask "255.255.0.0"
	;;			    #:gateway "192.168.9.99"
	;;			    #:name-servers '("192.168.12.254"))
	 (static-networking-service "enp5s0" "192.168.12.250"
				    #:netmask "255.255.0.0"
				    #:gateway "192.168.9.99"
				    #:name-servers '("192.168.12.254")))
   %mybase))

(operating-system
  (locale "en_US.utf8")
  (timezone "America/Los_Angeles")
  (keyboard-layout %mykb)

  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout %mykb)))
  (file-systems
    (cons* (file-system
             (mount-point "/home")
             (device
               (uuid "5683c522-49a8-45ea-b6d7-81532eb1c27e"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/")
             (device
               (uuid "8ba96afb-c0eb-4069-ae3a-3711eb672df2"
                     'ext4))
             (type "ext4"))
           %base-file-systems))
  (host-name "io")
  (users (cons* (user-account
                  (name "io")
                  (comment "io")
                  (group "users")
                  (home-directory "/home/io")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
   (append (list
	    acpid
	    avahi
	    bash
	    cpupower
	    cups
	    curl
	    dbus
	    dmenu
	    dropbear
	    ffmpeg
	    file
	    font-adobe-source-code-pro
	    fontconfig
	    font-dejavu
	    font-fira-code
	    font-gnu-freefont-ttf
	    font-gnu-unifont
	    font-hack
	    font-inconsolata
	    font-linuxlibertine
	    font-public-sans
	    font-terminus
	    font-xfree86-type1
	    git
	    httpd
	    i3-wm
	    iproute
	    linux-pam
	    lsof
	    ncurses
	    nss-certs
	    psmisc
	    rsync
	    setxkbmap
	    sqlite
	    strace
	    tlp
	    unbound
	    xbindkeys
	    xdg-user-dirs
	    xdg-utils
	    xdpyinfo
	    xev
	    xf86-input-evdev
	    xf86-input-libinput
	    xfontsel
	    xhost
	    xinit
	    xkbcomp
	    xmodmap
	    xorgproto
	    xorg-server
	    xrandr
	    xrdb
	    )
	   %base-packages))
  (services %minesvc)
  )

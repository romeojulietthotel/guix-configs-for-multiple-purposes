 Guix configs variety
=====================



`Guix configs for multiple purposes`

.. image:: http://guix.gnu.org/static/base/img/Guix.png
   :target: http://guix.gnu.org
   :alt: Discover Guix


**Features:**

- …Some configs as I learn how to declare systems for Guix.

AUTHORS
-------

Here's a list of developers to blame:

===================================  ====================================== ====================
*romeojulietthotel*                   https://gitlab.com/romeojulietthotel      2019-2139
===================================  ====================================== ====================

LICENSE
-------

These are licensed under the conditions of the
`GPLv3 <https://www.gnu.org/licenses/quick-guide-gplv3.html.en>`_.

DONATIONS
---------

If you think these saved you some serious time [*]_ you might consider 
a donation to somewhere that helps kids in a place where food and water
are scarce but western bombs are plentiful. The hardest part might be
finding a legitimate organization to donate to.

.. [*] Don't laugh, it's possible. [*]_
.. [*] Okay, maybe unlikely but...
